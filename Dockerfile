FROM openjdk:17-jdk-alpine
COPY dev-school-app-1.0-SNAPSHOT.jar dev-school-app-1.0-SNAPSHOT.jar
EXPOSE 8080/tcp
CMD ["java","-jar","dev-school-app-1.0-SNAPSHOT.jar"]
